<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://rextheme.com/
 * @since      1.0.0
 *
 * @package    Cypress
 * @subpackage Cypress/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Cypress
 * @subpackage Cypress/includes
 * @author     Saiduzzaman Tohin <tohin@coderex.co>
 */
class Cypress_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
